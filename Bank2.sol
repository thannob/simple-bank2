pragma solidity ^0.5.0;
import "https://github.com/OpenZeppelin/openzeppelin-contracts/blob/v2.5.0/contracts/math/SafeMath.sol";
import "https://github.com/OpenZeppelin/openzeppelin-contracts/blob/v2.5.0/contracts/ownership/Ownable.sol";

contract Bank is Ownable{
    using SafeMath for uint256;
    
    mapping(address => uint256) private balances;
    address[] accounts;
    uint256 rate = 3;
    //address public owner;
    
    constructor() public{
        //owner = msg.sender;
    }
    
    function deposite() public payable returns (uint256){
        if(0 == balances[msg.sender]){
            accounts.push(msg.sender);
        }
        // Use SafeMath
        //balances[msg.sender] = balances[msg.sender] + msg.value;
        balances[msg.sender] = balances[msg.sender].add(msg.value);
        return balances[msg.sender];
    }
    
    function withdraw(uint amount) public returns (uint256){
        require(balances[msg.sender] >= amount, "Balance is not enough");
        // Use SafeMath
        //balances[msg.sender] -= amount;
        balances[msg.sender] = balances[msg.sender].sub(amount);
        
        // call.value
        //msg.sender.transfer(amount);
        (bool success,) = msg.sender.call.value(amount)("");
        require(success, "Failed to transfer ether to user");
        
        return balances[msg.sender];
    }
    
    function userBalance() public view returns (uint256){
        return balances[msg.sender];
    }
    
    function systemBalance() public view returns (uint256){
        return address(this).balance;
    }
    
    
    function calculateInterest(address _user, uint256 _rate) private view returns(uint256){
        // User SafeMath
        //uint256 interest = balances[_user] * _rate / 100;
        uint256 interest = balances[_user].mul(_rate).div(100);
        return interest;
    }
    
    function totalInterestPerYear() external view returns(uint256){
        uint256 total = 0;
        for(uint256 i = 0; i < accounts.length; i++){
            address account = accounts[i];
            uint256 interest = calculateInterest(account, rate);
            // Use SafeMath
            //total += interest;
            total = total.add(interest);
        }
        return total;
    }
    
    function systemDeposite() public onlyOwner payable returns(uint256){
        //require(owner == msg.sender, "You are not manager");
        return systemBalance();
    }
    
    function systemWithdraw(uint amount)  public onlyOwner returns (uint256)  {
        //require(owner == msg.sender,"You are not manager");
        require(systemBalance() >= amount, "System balance is not enough");
        // call value
        //msg.sender.transfer(amount);
        (bool success,) = msg.sender.call.value(amount)("");
        require(success, "Failed to transfer ether to user");
        return systemBalance();
    }
    
    function payDividendsPerYear() payable public onlyOwner {
        //require(owner == msg.sender,"You are not manager");
        uint256 totalInterest = 0;
        for (uint256 i = 0 ; i< accounts.length ; i++){
            address account = accounts[i];
            uint256 interest = calculateInterest(account, rate);
            // Use SafeMath
            //balances[account] += interest;
            //totalInterest += interest;
            balances[account] = balances[account].add(interest);
            totalInterest = totalInterest.add(interest);
        }
        require(msg.value == totalInterest,"Not enough interest to pay");
    }
}
